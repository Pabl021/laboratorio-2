
infoPersonas = {}

def menu():
    '''En la funcion menu, estan disponibles las opciones que se le mostraran al usuario para que el escoja'''
    opcion = int(input("\033[4;36m"+"\n*****Informacion UTN*****\n"
                   "1. Registrar personas\n"
                   "2. Saber los nombres de las personas registradas\n"
                   "3. Eliminar una persona al ingresar la cedula\n"
                   "4. Consultar el nombre de una persona al registrar la cedula\n"
                   "5. Cambiar el nombre digitando la cedula\n"
                   "6. documentacion\n"
                   "7. Salir\n"
                   "Ingrese lo que desea escoger : \n"))


    def registroPer():
        '''En la funcion registroPer, lo que hacemos es solicitar la cantidad de personas y agregarlas la informacion al diccionario'''
    if opcion == 1:
        totalPer=int(input("\033[4;31m"+"Digite cuantas personas quiere agregar : "))

        for i in range(totalPer):
            cedula = int(input("\033[4;32m"+"Digite su numero de cedula : "))
            nombre = str(input("\033[4;33m"+"Digite su nombre completo : "))
            infoPersonas[cedula] = nombre
        return menu()

    def recorrerNom():
        '''En la funcion recorrerNom, lo que haremos es recorrer el diccionario e imprimir la informacion'''
    if opcion==2:
        for r in infoPersonas:
            print("\033[4;30m"+"Las personas inscritas en la lista son : " , infoPersonas)
            return menu()

    def eliminarPer():
        '''En la funcion eliminarPer, lo que realizaremos es eliminar una persona digitando el num de cedula'''
    if opcion==3:
        cedElim=int(input("\033[4;35m"+"Digite el numero de cedula que desea elimiar : "))
        infoPersonas.pop(cedElim)
        print("\033[4;32m"+"La lista actualizada es : " , infoPersonas)
        return menu()

    def consultarNom():
        '''En la funcion consultarNom, lo que pasara es que por medio del numero de cedula nos daremos cuenta del nombre de la persona'''
    if opcion==4:
        infoCed=int(input("\033[4;37m"+"Digite la cedula para saber a quien pertenece esta : "))
        for z in infoPersonas:
            infoPersonas.get(infoCed)
        print("\033[4;38m"+"la persona que pertenece a ese numero de cedula es : " , infoPersonas[infoCed])
        return menu()

    def cambiarNom():
        '''En la funcion cambiarNom, lo que haremos es que con digitar el numero de cedula pueda cambiar su nombre'''
    if opcion==5:
        infoCed = int(input("\033[4;39m"+"Digite la cedula de la informacion que desea cambiar : "))
        infoNueva=str(input("\033[4;30m"+"Digte el nuevo nombre que desea darle a su numero de cedula : "))
        for y in infoPersonas:
            infoPersonas[infoCed]=infoNueva
        print("\033[4;31m"+"La lista actualizada con el nombre nuevo es : " , infoPersonas)
        return menu()

    def salir():
        '''En la funcion salir, le daremos las gracias al usuario y se terminara el programa'''
    if opcion==7:
        print("\033[4;31m"+"¡¡¡Gracias por usar nuestro programa!!! ¡¡¡Has terminado!!!")

    def documentacion():
        '''En la funcion documentacion, estan disponibles todas las documentaciones anteriores'''
    if opcion==6:
        print("\033[4;32m"+"",menu.__doc__)
        print("\033[4;32m"+"",registroPer.__doc__)
        print("\033[4;32m"+"",recorrerNom.__doc__)
        print("\033[4;32m"+"",eliminarPer.__doc__)
        print("\033[4;32m"+"",consultarNom.__doc__)
        print("\033[4;32m"+"",cambiarNom.__doc__)
        print("\033[4;32m"+"",documentacion.__doc__)
        print("\033[4;32m"+"",salir.__doc__)
        return menu()
menu()



